#!/usr/bin/env tsx
import process from 'node:process';

import autocannon from 'autocannon';

const url = process.env.PACTUM_REQUEST_BASE_URL ?? 'http://localhost:1337';
const instance = autocannon(
  {
    title: 'Stress test the API',
    url,
    connections: 100,
    workers: 20,
    duration: 60,
    requests: [
      {
        method: 'GET',
        path: '/health-check',
      },
      {
        method: 'POST',
        path: '/tasks',
        body: JSON.stringify({
          title: 'Create a task',
        }),
        headers: {
          'Content-Type': 'application/json',
        },
      },
      {
        method: 'GET',
        path: '/tasks?limit=100',
      },
    ],
  },
  // eslint-disable-next-line promise/prefer-await-to-callbacks
  (error) => {
    if (error) {
      throw error;
    }
  },
);

// this is used to kill the instance on CTRL-C
process.once('SIGINT', () => {
  // @ts-expect-error missing from typings
  instance.stop();
});

autocannon.track(instance, { renderResultsTable: true });
