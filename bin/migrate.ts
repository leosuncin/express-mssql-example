#!/usr/bin/env tsx
import path from 'node:path';
import process from 'node:process';
import { format, parseArgs } from 'node:util';

import sql from 'mssql';
import Postgrator from 'postgrator';

import { dbConfig } from '../src/config/db';

const args = parseArgs({
  args: process.argv.slice(2),
  options: {
    target: {
      type: 'string',
    },
  },
});

async function migrate(options: typeof args.values) {
  try {
    const pool = await sql.connect({
      ...dbConfig,
      requestTimeout: 15_000,
      connectionTimeout: 15_000,
      pool: {
        max: 1,
        min: 1,
      },
    });
    const postgrator = new Postgrator({
      migrationPattern: path.resolve(process.cwd(), 'migrations', '*'),
      driver: 'mssql',
      database: 'master',
      async execQuery(query) {
        const result = await pool.batch(query);

        return { rows: result.recordset };
      },
    });

    await postgrator.migrate(options.target);

    await pool.close();
  } catch (error) {
    process.stderr.write(`${format(error)}\n`);
    process.exit(1);
  }
}

void migrate(args.values);
