#!/usr/bin/env tsx
import { apiClient } from '@japa/api-client';
import { expect } from '@japa/expect';
import { runFailedTests } from '@japa/run-failed-tests';
import { configure, processCliArgs, run } from '@japa/runner';
import { specReporter } from '@japa/spec-reporter';

/*
|--------------------------------------------------------------------------
| Configure tests
|--------------------------------------------------------------------------
|
| The configure method accepts the configuration to configure the Japa
| tests runner.
|
| The first method call "processCliArgs" process the command line arguments
| and turns them into a config object. Using this method is not mandatory.
|
| Please consult japa.dev/runner-config for the config docs.
*/
configure({
  ...processCliArgs(process.argv.slice(2)),

  plugins: [
    expect(),
    runFailedTests(),
    apiClient(`http://localhost:${process.env.PORT ?? 1337}`),
  ],
  suites: [
    {
      name: 'unit',
      files: ['src/**/*.{spec,test}.ts'],
    },
    {
      name: 'e2e',
      files: ['tests/**/*.{spec,test}.ts'],
    },
  ],
  reporters: [specReporter()],
  // eslint-disable-next-line import/dynamic-import-chunkname
  importer: (filePath) => import(filePath),
});

/*
|--------------------------------------------------------------------------
| Run tests
|--------------------------------------------------------------------------
|
| The following "run" method is required to execute all the tests.
|
*/
void run();
