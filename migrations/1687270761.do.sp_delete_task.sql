CREATE PROCEDURE todo.sp_delete_task(@p_id bigint)
  AS BEGIN
  DELETE FROM todo.tasks OUTPUT DELETED.*
  WHERE id = @p_id;
END;
