CREATE TABLE sales.stores (
  store_id bigint IDENTITY (1, 1) PRIMARY KEY,
  store_name varchar(255) NOT NULL,
  phone varchar(25),
  email varchar(255),
  street varchar(255),
  city varchar(255),
  state varchar(10),
  zip_code varchar(5)
)
