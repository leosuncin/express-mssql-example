CREATE TABLE production.products (
  product_id bigint IDENTITY (1, 1) PRIMARY KEY,
  product_name varchar(255) NOT NULL,
  brand_id bigint NOT NULL,
  category_id bigint NOT NULL,
  model_year smallint NOT NULL,
  list_price DECIMAL(10, 2) NOT NULL,
  FOREIGN KEY (category_id) REFERENCES production.categories(category_id) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (brand_id) REFERENCES production.brands(brand_id) ON DELETE CASCADE ON UPDATE CASCADE
)
