CREATE TABLE sales.orders (
  order_id bigint IDENTITY (1, 1) PRIMARY KEY,
  customer_id bigint,
  order_status tinyint NOT NULL,
  -- Order status: 1 = Pending; 2 = Processing; 3 = Rejected; 4 = Completed
  order_date date NOT NULL,
  required_date date NOT NULL,
  shipped_date date,
  store_id bigint NOT NULL,
  staff_id bigint NOT NULL,
  FOREIGN KEY (customer_id) REFERENCES sales.customers(customer_id) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (store_id) REFERENCES sales.stores(store_id) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (staff_id) REFERENCES sales.staffs(staff_id) ON DELETE NO ACTION ON UPDATE NO ACTION
)
