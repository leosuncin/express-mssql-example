CREATE TABLE sales.customers (
  customer_id bigint IDENTITY (1, 1) PRIMARY KEY,
  first_name varchar(255) NOT NULL,
  last_name varchar(255) NOT NULL,
  phone varchar(25),
  email varchar(255) NOT NULL,
  street varchar(255),
  city varchar(50),
  state varchar(25),
  zip_code varchar(5)
)
