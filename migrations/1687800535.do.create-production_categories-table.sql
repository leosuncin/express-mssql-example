CREATE TABLE production.categories (
  category_id bigint IDENTITY (1, 1) PRIMARY KEY,
  category_name varchar(255) NOT NULL
)
