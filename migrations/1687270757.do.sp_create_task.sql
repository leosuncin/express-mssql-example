CREATE PROCEDURE todo.sp_create_task(@p_title varchar(MAX))
  AS BEGIN
  INSERT INTO todo.tasks(title) OUTPUT INSERTED.*
    VALUES(@p_title);
END;
