CREATE TABLE production.brands (
  brand_id bigint IDENTITY (1, 1) PRIMARY KEY,
  brand_name varchar(255) NOT NULL
)
