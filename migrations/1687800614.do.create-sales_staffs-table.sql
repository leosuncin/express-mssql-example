CREATE TABLE sales.staffs (
  staff_id bigint IDENTITY (1, 1) PRIMARY KEY,
  first_name varchar(50) NOT NULL,
  last_name varchar(50) NOT NULL,
  email varchar(255) NOT NULL UNIQUE,
  phone varchar(25),
  active tinyint NOT NULL,
  store_id bigint NOT NULL,
  manager_id bigint,
  FOREIGN KEY (store_id) REFERENCES sales.stores(store_id) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (manager_id) REFERENCES sales.staffs(staff_id) ON DELETE NO ACTION ON UPDATE NO ACTION
)
