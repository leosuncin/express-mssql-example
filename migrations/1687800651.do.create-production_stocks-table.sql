CREATE TABLE production.stocks (
  store_id bigint,
  product_id bigint,
  quantity int,
  PRIMARY KEY (store_id, product_id),
  FOREIGN KEY (store_id) REFERENCES sales.stores(store_id) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (product_id) REFERENCES production.products(product_id) ON DELETE CASCADE ON UPDATE CASCADE
)
