SET IDENTITY_INSERT production.categories ON;

INSERT INTO production.categories(category_id, category_name)
  VALUES (1, 'Children Bicycles'),
(2, 'Comfort Bicycles'),
(3, 'Cruisers Bicycles'),
(4, 'Cyclocross Bicycles'),
(5, 'Electric Bikes'),
(6, 'Mountain Bikes'),
(7, 'Road Bikes');

SET IDENTITY_INSERT production.categories OFF;
