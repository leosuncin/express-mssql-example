import { test } from '@japa/runner';
import { StatusCodes } from 'http-status-codes';

test.group('Task routes', () => {
  test('create a new task', async ({ client, expect }) => {
    const response = await client.post('/tasks').json({ title: 'test' });
    const task = response.body();

    // status code is 201 Created
    expect(response.status()).toEqual(StatusCodes.CREATED);
    // task title is test
    expect(task).toHaveProperty('title', 'test');
    // task is not completed
    expect(task).toHaveProperty('completed', false);
  });

  test('list all tasks', async ({ client, expect }) => {
    const response = await client.get('/tasks');
    const page = await response.body();

    // status code is 200 OK
    expect(response.status()).toEqual(StatusCodes.OK);
    // response body is an array
    expect(Array.isArray(page.data)).toBe(true);
    // response body has a meta property
    expect(page).toHaveProperty('meta', expect.anything());
  });

  test('get a task by id', async ({ client, expect }) => {
    const response = await client.get('/tasks/1');
    const task = response.body();

    // status code is 200 OK
    expect(response.status()).toEqual(StatusCodes.OK);
    // task id is 1
    expect(task).toHaveProperty('id', '1');
  });

  test('update a task by id', async ({ client, expect }) => {
    const response = await client
      .patch('/tasks/1')
      .json({ title: 'changed', completed: true });
    const task = response.body();

    // status code is 200 OK
    expect(response.status()).toEqual(StatusCodes.OK);
    // task id is 1
    expect(task).toHaveProperty('id', '1');
    // task title is changed
    expect(task).toHaveProperty('title', 'changed');
    // task is completed
    expect(task).toHaveProperty('completed', true);
  });

  test('delete a task by id', async ({ client, expect }) => {
    let response = await client.post('/tasks').json({ title: 'delete me' });
    const task: Record<string, string | boolean> = response.body();
    response = await client.delete(`/tasks/${task.id}`);

    // status code is 200 OK
    expect(response.status()).toEqual(StatusCodes.OK);
  });
});
