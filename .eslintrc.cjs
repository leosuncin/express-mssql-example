const { createConfig } = require('eslint-config-galex/dist/createConfig');

module.exports = createConfig({
  overrides: [
    {
      files: ['*.ts', '*.js'],
      rules: {
        'new-cap': 'off',
      },
    },
  ],
});
