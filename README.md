
# Ejemplo de aplicación de Express

Muestra como conectarse a SQL Server usando [mssql](https://tediousjs.github.io/node-mssql/) y no morir en el intento

## Instalación

Clonar el repositorio e instalar las dependencias

```bash
git clone https://gitlab.com/leosuncin/express-mssql-example.git
cd express-mssql-example
pnpm install
```
    
Iniciar la instancia local de SQL Server usando Docker Compose

```bash
docker compose up -d
```

Esperar hasta que SQL Server acepte conexiones (cuando el contenedor este en estado healthy) para aplicar las migraciones

```bash
pnpm migrate
```

Iniciar el servidor en modo de desarrollo

```bash
pnpm dev
```
## Ejecutar el proyecto

Clonar el repositorio en local

```bash
git clone https://gitlab.com/leosuncin/express-mssql-example.git
```

Cambiarse al directorio del proyecto

```bash
cd express-mssql-example
```

Instalar las dependencias con PNPM

```bash
pnpm install
```
    
Iniciar la instancia local de SQL Server usando Docker Compose

```bash
docker compose up -d
```

Esperar hasta que SQL Server acepte conexiones (cuando el contenedor este en estado healthy) para aplicar las migraciones

```bash
pnpm migrate
```

Iniciar el servidor en modo de desarrollo

```bash
pnpm dev
```
## Ejecutar las pruebas

Para ejecutar las pruebas E2E (el servidor debe estar corriendo), ejecutar el siguiente comando

```bash
  pnpm test
```

Para ejecutar pruebas de stress, ejecutar el siguiente comando

```bash
pnpm test:stress
```

## Licencia

Este proyecto está protegido por [GNU General Public License v3.0](https://choosealicense.com/licenses/gpl-3.0/)

