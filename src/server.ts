import { createServer } from 'node:http';

import express from 'express';
import paginate from 'express-paginate';
import openapi from 'openapi-comment-parser';
import swaggerUi from 'swagger-ui-express';

import { env } from './config/env';
import { killSwitch } from './controllers/app.controller';
import { errorHandlerMiddleware } from './middlewares/error-handler.middleware';
import { middleware as loggerMiddleware } from './middlewares/logger.middleware';
import { notFoundRouteMiddleware } from './middlewares/not-found-route.middleware';
import { taskRouter } from './routes/tasks';
import { handleExceptions } from './utils/handle-exceptions';

export const app = express();

const spec = env.isProd ? require('./openapi.json') : openapi(); // eslint-disable-line @typescript-eslint/no-require-imports

app.use(
  loggerMiddleware,
  express.json(),
  express.urlencoded({ extended: true }),
  paginate.middleware(10, 100),
);
app.use('/docs', swaggerUi.serve, swaggerUi.setup(spec));
app.options('/kill-switch', killSwitch);
app.use('/tasks', taskRouter);
app.use(notFoundRouteMiddleware);
app.use(errorHandlerMiddleware);

export const server = createServer(app);

server.on('error', handleExceptions);
