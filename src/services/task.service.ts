import mssql from 'mssql';

import {
  type RawTask as Task,
  rawTaskDto,
  rawTasksWithTotalDto,
} from '../dtos/task.dto';
import { type UpdateTask } from '../dtos/update-task.dto';

type Page = {
  limit: number;
  offset: number;
};

export class TaskService {
  #pool: mssql.ConnectionPool;

  public constructor(pool: mssql.ConnectionPool) {
    this.#pool = pool;
  }

  public async create(title: Task['title']): Promise<Task> {
    const request = this.#pool.request();

    const result = await request
      .input('p_title', mssql.VarChar(mssql.MAX), title)
      .execute('todo.sp_create_task');
    const [task] = result.recordset;

    return rawTaskDto.parse(task);
  }

  public async list({ limit = 100, offset = 0 }: Partial<Page> = {}): Promise<
    [Task[], number]
  > {
    const request = this.#pool.request();
    const result = await request
      .input('p_limit', mssql.Int, limit)
      .input('p_offset', mssql.Int, offset)
      .output('out_total', mssql.Int)
      .execute('todo.sp_get_tasks');

    return rawTasksWithTotalDto.parse([
      result.recordset,
      result.output.out_total,
    ]);
  }

  public async get(id: bigint): Promise<Task | undefined> {
    const request = this.#pool.request();
    const result = await request
      .input('p_id', mssql.BigInt, id)
      .execute('todo.sp_get_task');
    const [task] = result.recordset;

    return rawTaskDto.optional().parse(task);
  }

  public async update(
    id: bigint,
    change: UpdateTask,
  ): Promise<Task | undefined> {
    const request = this.#pool.request();
    const result = await request
      .input('p_id', mssql.BigInt, id)
      .input('p_title', mssql.VarChar(mssql.MAX), change.title)
      .input('p_completed', mssql.Bit, change.completed)
      .execute('todo.sp_update_task');
    const [task] = result.recordset;

    return rawTaskDto.optional().parse(task);
  }

  public async delete(id: bigint): Promise<Task | undefined> {
    const request = this.#pool.request();
    const result = await request
      .input('p_id', mssql.BigInt, id)
      .execute('todo.sp_delete_task');
    const [task] = result.recordset;

    return rawTaskDto.optional().parse(task);
  }
}
