import { test } from '@japa/runner';
import { It, Mock } from 'moq.ts';
import { type ConnectionPool, type Request } from 'mssql';

import { TaskService } from './task.service';

test.group('TaskService', (group) => {
  let service: TaskService;

  group.each.setup(() => {
    const task = {
      id: '1',
      title: 'test',
      completed: false,
      created_at: '2023-06-25T23:23:07.350Z',
      updated_at: '2023-06-25T23:23:07.350Z',
    };

    const requestMocked: Request = new Mock<Request>()
      .setup((request) =>
        request.input(
          It.Is<string>((name) => typeof name === 'string'),
          It.IsAny(),
          It.IsAny(),
        ),
      )
      .callback(() => requestMocked)
      .setup((request) => request.output('out_total', It.IsAny()))
      .callback(() => requestMocked)
      .setup((request) => request.execute('todo.sp_create_task'))
      .returnsAsync({
        // @ts-expect-error fake recordset
        recordset: [task],
        rowsAffected: [1],
        output: {},
      })
      .setup((request) => request.execute('todo.sp_get_tasks'))
      .returnsAsync({
        // @ts-expect-error fake recordset
        recordset: [task],
        rowsAffected: [1, 1],
        output: {
          out_total: 1,
        },
      })
      .setup((request) => request.execute('todo.sp_get_task'))
      .returnsAsync({
        // @ts-expect-error fake recordset
        recordset: [task],
        rowsAffected: [1],
        output: {},
      })
      .setup((request) => request.execute('todo.sp_update_task'))
      .returnsAsync({
        // @ts-expect-error fake recordset
        recordset: [{ ...task, completed: true }],
        rowsAffected: [1],
        output: {},
      })
      .setup((request) => request.execute('todo.sp_delete_task'))
      .returnsAsync({
        // @ts-expect-error fake recordset
        recordset: [task],
        rowsAffected: [1],
        output: {},
      })
      .object();

    const poolMocked: ConnectionPool = new Mock<ConnectionPool>()
      .setup((instance) => instance.request())
      .returns(requestMocked)
      .object();

    service = new TaskService(poolMocked);
  });

  test('create an instance of TaskService', ({ expect }) => {
    expect(service).toBeInstanceOf(TaskService);
  });

  test('save a new task', async ({ expect }) => {
    const title = 'test';

    const task = await service.create(title);

    expect(task).toHaveProperty('id');
    expect(task).toHaveProperty('title', title);
    expect(task).toHaveProperty('completed', false);
  });

  test('get all tasks', async ({ expect }) => {
    const [tasks, total] = await service.list();

    expect(tasks).toHaveLength(1);
    expect(total).toBe(1);
  });

  test('get one task by id', async ({ expect }) => {
    const task = await service.get(BigInt('1'));

    expect(task).toHaveProperty('id', 1n);
    expect(task).toHaveProperty('title');
    expect(task).toHaveProperty('completed');
  });

  test('update one task by id', async ({ expect }) => {
    const change = { completed: true };
    const task = await service.update(BigInt('1'), change);

    expect(task).toHaveProperty('id', 1n);
    expect(task).toHaveProperty('title');
    expect(task).toHaveProperty('completed', change.completed);
  });

  test('delete one task by id', async ({ expect }) => {
    const task = await service.delete(BigInt('1'));

    expect(task).toHaveProperty('id', 1n);
    expect(task).toHaveProperty('title');
    expect(task).toHaveProperty('completed');
  });
});
