import { type RequestHandler } from 'express';
import { StatusCodes } from 'http-status-codes';

export const killSwitch: RequestHandler = (_, response) => {
  // KILL THE CONNECTION TO THE DATABASE
  void response.app.locals.db.close();

  response.status(StatusCodes.SERVICE_UNAVAILABLE).send('Helter Skelter');

  // roll the dice
  if (Math.random() > 0.5) {
    // KILL THE SERVER
    process.exit(1); // eslint-disable-line unicorn/no-process-exit
  }
};
