import { type Request, type Response } from 'express';
import createHttpError from 'http-errors';
import { StatusCodes } from 'http-status-codes';

import { paginatedTask, taskDto } from '../dtos/task.dto';
import { TaskService } from '../services/task.service';

/**
 * POST /tasks
 * @summary Create a new task
 * @description Saves a new uncompleted task.
 * @bodyContent {CreateTask} application/json
 * @bodyRequired
 * @response 201 - The created task
 * @responseContent {Task} 201.application/json
 * @response 400 - Invalid request body
 */
export async function create(
  request: Request,
  response: Response,
): Promise<void> {
  const service = new TaskService(response.app.locals.db);
  const task = await service.create(request.body.title);

  response.status(StatusCodes.CREATED).json(taskDto.parse(task));
}

/**
 * GET /tasks
 * @summary Returns the list of all the tasks
 * @description The list is paginated.
 * Use the `limit` and `page` query parameters to control pagination.
 * @queryParam {integer} [page=1] - The number of items to skip before starting to collect the result set
 * @queryParam {integer} [limit=10] - The numbers of items to return
 * @response 200 - A JSON array of tasks
 * @responseContent {PaginateTasks} 200.application/json
 */
export async function list(
  request: Request,
  response: Response,
): Promise<void> {
  const service = new TaskService(response.app.locals.db);
  const itemsPerPage = Number(request.query.limit);
  const currentPage = Number(request.query.page);
  const [tasks, totalItems] = await service.list({
    offset: request.offset,
    limit: itemsPerPage,
  });
  const itemCount = tasks.length;
  const totalPages = Math.ceil(totalItems / itemsPerPage);

  response.json(
    paginatedTask.parse({
      data: tasks,
      meta: {
        itemCount,
        totalItems,
        itemsPerPage,
        totalPages,
        currentPage,
      },
    }),
  );
}

/**
 * GET /tasks/{id}
 * @summary Returns a task by id
 * @description Returns the task with the id specified
 * @pathParam {integer} id - Numeric ID of the task to get.
 * @response 200 - The specific task
 * @responseContent {Task} 200.application/json
 * @response 404 - Task not found
 */
export async function get(request: Request, response: Response): Promise<void> {
  const service = new TaskService(response.app.locals.db);
  const task = await service.get(BigInt(request.params.id));

  if (!task) {
    throw createHttpError.NotFound(
      `Task not found with id ${request.params.id}`,
    );
  }

  response.json(taskDto.parse(task));
}

/**
 * PATCH /tasks/{id}
 * @summary Updates a task
 * @description Updates the task with the id specified
 * @pathParam {integer} id - Numeric ID of the task to update.
 * @bodyContent {UpdateTask} application/json
 * @bodyRequired
 * @response 200 - The updated task
 * @responseContent {Task} 200.application/json
 * @response 400 - Invalid request body
 * @response 404 - Task not found
 */
export async function update(
  request: Request,
  response: Response,
): Promise<void> {
  const service = new TaskService(response.app.locals.db);
  const task = await service.update(BigInt(request.params.id), request.body);

  if (!task) {
    throw createHttpError.NotFound(
      `Task not found with id ${request.params.id}`,
    );
  }

  response.json(taskDto.parse(task));
}

/**
 * DELETE /tasks/{id}
 * @summary Removes a task
 * @description Removes the task with the id specified
 * @pathParam {integer} id - Numeric ID of the task to remove.
 * @response 200 - The removed task
 * @responseContent {Task} 200.application/json
 * @response 404 - Task not found
 */
export async function remove(
  request: Request,
  response: Response,
): Promise<void> {
  const service = new TaskService(response.app.locals.db);
  const task = await service.delete(BigInt(request.params.id));

  if (!task) {
    throw createHttpError.NotFound(
      `Task not found with id ${request.params.id}`,
    );
  }

  response.json(taskDto.parse(task));
}
