import { test } from '@japa/runner';
import { type Application } from 'express';
import { StatusCodes } from 'http-status-codes';
import { It, Mock } from 'moq.ts';
import { type ConnectionPool, type Request } from 'mssql';
import { createMocks } from 'node-mocks-http';

import { create, get, list, remove, update } from './task.controller';

test.group('TaskController', (group) => {
  let poolMocked: ConnectionPool;

  group.each.setup(() => {
    const task = {
      id: '1',
      title: 'test',
      completed: false,
      created_at: '2023-06-25T23:23:07.350Z',
      updated_at: '2023-06-25T23:23:07.350Z',
    };

    const requestMocked: Request = new Mock<Request>()
      .setup((request) =>
        request.input(
          It.Is<string>((name) => typeof name === 'string'),
          It.IsAny(),
          It.IsAny(),
        ),
      )
      .callback(() => requestMocked)
      .setup((request) => request.output('out_total', It.IsAny()))
      .callback(() => requestMocked)
      .setup((request) => request.execute('todo.sp_create_task'))
      .returnsAsync({
        // @ts-expect-error fake recordset
        recordset: [task],
        rowsAffected: [1],
        output: {},
      })
      .setup((request) => request.execute('todo.sp_get_tasks'))
      .returnsAsync({
        // @ts-expect-error fake recordset
        recordset: [task],
        rowsAffected: [1, 1],
        output: {
          out_total: 1,
        },
      })
      .setup((request) => request.execute('todo.sp_get_task'))
      .returnsAsync({
        // @ts-expect-error fake recordset
        recordset: [task],
        rowsAffected: [1],
        output: {},
      })
      .setup((request) => request.execute('todo.sp_update_task'))
      .returnsAsync({
        // @ts-expect-error fake recordset
        recordset: [{ ...task, completed: true }],
        rowsAffected: [1],
        output: {},
      })
      .setup((request) => request.execute('todo.sp_delete_task'))
      .returnsAsync({
        // @ts-expect-error fake recordset
        recordset: [task],
        rowsAffected: [1],
        output: {},
      })
      .object();

    poolMocked = new Mock<ConnectionPool>()
      .setup((instance) => instance.request())
      .returns(requestMocked)
      .object();
  });

  test('create one task', async ({ expect }) => {
    const { req, res } = createMocks({
      method: 'POST',
      path: '/tasks',
      body: { title: 'test' },
    });
    res.app = { locals: { db: poolMocked } } as unknown as Application;

    await create(req, res);

    expect(res._getStatusCode()).toBe(StatusCodes.CREATED);
    expect(res._getJSONData()).toHaveProperty('id');
    expect(res._getJSONData()).toHaveProperty('title', req.body.title);
    expect(res._getJSONData()).toHaveProperty('completed', false);
  });

  test('paginate all tasks', async ({ expect }) => {
    const { req, res } = createMocks({
      method: 'GET',
      path: '/tasks',
      query: { page: 1, limit: 10 },
    });
    res.app = { locals: { db: poolMocked } } as unknown as Application;

    await list(req, res);

    expect(res._getStatusCode()).toBe(StatusCodes.OK);
    expect(res._getJSONData()).toHaveProperty('data');
    expect(res._getJSONData()).toHaveProperty('meta');
  });

  test('get one task', async ({ expect }) => {
    const { req, res } = createMocks({
      method: 'GET',
      path: '/tasks/1',
      params: { id: '1' },
    });
    res.app = { locals: { db: poolMocked } } as unknown as Application;

    await get(req, res);

    expect(res._getStatusCode()).toBe(StatusCodes.OK);
    expect(res._getJSONData()).toHaveProperty('id');
    expect(res._getJSONData()).toHaveProperty('title');
    expect(res._getJSONData()).toHaveProperty('completed');
  });

  test('update one task', async ({ expect }) => {
    const { req, res } = createMocks({
      method: 'PUT',
      path: '/tasks/1',
      params: { id: '1' },
      body: { completed: true },
    });
    res.app = { locals: { db: poolMocked } } as unknown as Application;

    await update(req, res);

    expect(res._getStatusCode()).toBe(StatusCodes.OK);
    expect(res._getJSONData()).toHaveProperty('id');
    expect(res._getJSONData()).toHaveProperty('title');
    expect(res._getJSONData()).toHaveProperty('completed', true);
  });

  test('delete one task', async ({ expect }) => {
    const { req, res } = createMocks({
      method: 'DELETE',
      path: '/tasks/1',
      params: { id: '1' },
    });
    res.app = { locals: { db: poolMocked } } as unknown as Application;

    await remove(req, res);

    expect(res._getJSONData()).toHaveProperty('id');
    expect(res._getJSONData()).toHaveProperty('title');
    expect(res._getJSONData()).toHaveProperty('completed');
  });
});
