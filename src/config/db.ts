import { type config } from 'mssql';

import { env } from './env';

export const dbConfig: config = {
  user: env.MSSQL_USER,
  password: env.MSSQL_PASSWORD,
  server: env.MSSQL_SERVER,
  port: env.MSSQL_PORT,
  database: env.MSSQL_DATABASE,
  options: {
    encrypt: false,
    trustServerCertificate: true,
    enableArithAbort: true,
  },
};
