import process from 'node:process';

import { cleanEnv, port, str } from 'envalid';

type NODE_ENV = 'development' | 'production' | 'test' | 'ci';

declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace NodeJS {
    // eslint-disable-next-line @typescript-eslint/consistent-type-definitions
    interface ProcessEnv {
      NODE_ENV: NODE_ENV;
      PORT: string;
      MSSQL_PORT: string;
      MSSQL_SERVER: string;
      MSSQL_USER: string;
      MSSQL_PASSWORD: string;
      MSSQL_DATABASE: string;
    }
  }
}

process.env.NODE_ENV ??= 'development';

export const env = cleanEnv(process.env, {
  NODE_ENV: str<NODE_ENV>({
    choices: ['development', 'production', 'test', 'ci'],
  }),
  PORT: port({ default: 1337 }),
  MSSQL_PORT: port({ default: 1433 }),
  MSSQL_SERVER: str({ default: 'localhost' }),
  MSSQL_USER: str({ devDefault: 'sa' }),
  MSSQL_PASSWORD: str({ devDefault: '5DHeMDfwVN' }),
  MSSQL_DATABASE: str({ devDefault: 'master  ' }),
});
