import { Router } from 'express';
import asyncHandler from 'express-async-handler';

import {
  create,
  get,
  list,
  remove,
  update,
} from '../controllers/task.controller';
import { createTaskDto } from '../dtos/create-task.dto';
import { taskIdDto } from '../dtos/task-id.dto';
import { updateTaskDto } from '../dtos/update-task.dto';
import { validateMiddleware } from '../middlewares/validate.middleware';

export const taskRouter = Router();

taskRouter
  .route('/')
  .post(validateMiddleware({ body: createTaskDto }), asyncHandler(create))
  .get(asyncHandler(list));

taskRouter
  .route('/:id')
  .get(validateMiddleware({ params: taskIdDto }), asyncHandler(get))
  .patch(
    validateMiddleware({ params: taskIdDto, body: updateTaskDto }),
    asyncHandler(update),
  )
  .delete(validateMiddleware({ params: taskIdDto }), asyncHandler(remove));
