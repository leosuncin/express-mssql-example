import { type Output } from 'doubter';

import { rawTaskDto } from './task.dto';

export const updateTaskDto = rawTaskDto.pick(['title', 'completed']).partial();

export type UpdateTask = Output<typeof updateTaskDto>;
