import {
  any,
  array,
  bigint,
  boolean,
  date,
  number,
  object,
  type Output,
  record,
  string,
  tuple,
} from 'doubter';

type CamelCase<S extends string> =
  S extends `${infer P1}_${infer P2}${infer P3}`
    ? `${Lowercase<P1>}${Uppercase<P2>}${CamelCase<P3>}`
    : Lowercase<S>;

function camelCase<Key extends string>(str: Key): CamelCase<Key> {
  return str.replace(/_([a-z])/gu, (g) => g[1].toUpperCase()) as CamelCase<Key>;
}

export const rawTaskDto = object({
  id: bigint().coerce(),
  title: string().min(1),
  completed: boolean().coerce(),
  created_at: date().coerce(),
  updated_at: date().coerce(),
}).strip();

export type RawTask = Output<typeof rawTaskDto>;

export const rawTasksWithTotalDto = tuple([array(rawTaskDto), number().gte(0)]);

const taskKeys = rawTaskDto.keyof().transform((value) => camelCase(value));

export const taskDto = rawTaskDto
  .extend({
    id: bigint().transform((value) => value.toString()),
    created_at: date().transform((value) => value.toISOString()),
    updated_at: date().transform((value) => value.toISOString()),
  })
  .to(record(taskKeys, any()));

export type Task = Output<typeof taskDto>;

export const paginatedTask = object({
  data: array(rawTaskDto).to(array(taskDto)),
  meta: object({
    itemCount: number().coerce().gte(0),
    totalItems: number().coerce().gte(0),
    itemsPerPage: number().coerce().gte(0),
    totalPages: number().coerce().gte(1),
    currentPage: number().coerce().gte(1),
  }),
});
