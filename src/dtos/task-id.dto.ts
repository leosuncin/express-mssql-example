import { bigint, object, string } from 'doubter';

export const taskIdDto = object({
  id: string()
    .regex(/\d+/u, { message: 'Must be an integer' })
    .to(bigint().coerce()),
}).strip();
