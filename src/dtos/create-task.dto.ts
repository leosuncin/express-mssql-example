import { object, type Output, string } from 'doubter';

export const createTaskDto = object({
  title: string().min(1),
}).strip();

export type CreateTask = Output<typeof createTaskDto>;
