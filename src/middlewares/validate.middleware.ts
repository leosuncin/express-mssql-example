import { type AnyShape, type Output, ValidationError } from 'doubter';
import { type RequestHandler } from 'express';
import { ReasonPhrases, StatusCodes } from 'http-status-codes';

import { invariant } from '../utils/invariant';

type Options = Partial<Record<'body' | 'params' | 'query', AnyShape>>;

function validate<DTO extends AnyShape>(
  input: unknown,
  dto: DTO,
): Promise<Output<DTO>> | Output<DTO> {
  return dto.isAsync ? dto.parseAsync(input) : dto.parse(input);
}

export const validateMiddleware =
  (options: Options): RequestHandler =>
  async (request, response, next) => {
    try {
      if (options.body) {
        // eslint-disable-next-line require-atomic-updates
        request.body = await validate(request.body, options.body);
      }

      if (options.params) {
        // eslint-disable-next-line require-atomic-updates
        request.params = await validate(request.params, options.params);
      }

      if (options.query) {
        // eslint-disable-next-line require-atomic-updates
        request.query = await validate(request.query, options.query);
      }

      next();
    } catch (error) {
      if (error instanceof ValidationError) {
        response.status(StatusCodes.BAD_REQUEST).json({
          name: ReasonPhrases.BAD_REQUEST,
          statusCode: StatusCodes.BAD_REQUEST,
          message: error.issues
            .map((issue) => {
              invariant(Array.isArray(issue.path));
              invariant(typeof issue.message === 'string');

              return `${issue.path.join('.')}: ${issue.message}`;
            })
            .join('\n'),
          issues: error.issues,
        });
      } else {
        next(error);
      }
    }
  };
