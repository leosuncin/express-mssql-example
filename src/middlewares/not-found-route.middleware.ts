import { type RequestHandler } from 'express';
import createHttpError from 'http-errors';

export const notFoundRouteMiddleware: RequestHandler = (
  request,
  _response,
  next,
) => {
  next(createHttpError.NotFound(`Cannot ${request.method} ${request.url}`));
};
