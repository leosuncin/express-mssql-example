import process from 'node:process';

import { createTerminus } from '@godaddy/terminus';
import mssql from 'mssql';

import { dbConfig } from './config/db';
import { env } from './config/env';
import { logger } from './middlewares/logger.middleware';
import { app, server } from './server';
import { handleExceptions } from './utils/handle-exceptions';

declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace Express {
    // eslint-disable-next-line @typescript-eslint/consistent-type-definitions
    interface Locals {
      db: mssql.ConnectionPool;
    }
  }
}

process.on('unhandledRejection', handleExceptions);
process.on('uncaughtException', handleExceptions);

const defaults = { port: env.PORT, db: dbConfig } as const;

export async function bootstrap(options = defaults): Promise<void> {
  const db = await mssql.connect(options.db);

  createTerminus(server, {
    signals: ['SIGINT', 'SIGTERM'],
    healthChecks: {
      '/health-check': () =>
        Promise.all([
          // eslint-disable-next-line promise/prefer-await-to-then
          db.query("SELECT 'up' AS db").then((result) => result.recordset[0]),
        ]),
    },
    onSignal() {
      return Promise.all([db.close()]);
    },
    logger: logger.fatal.bind(logger),
  });

  server.listen(options.port, () => {
    logger.info(`Server is listening on http://localhost:${options.port}`);
  });

  app.locals.db = db;
}

if (require.main === module) {
  void bootstrap();
}
